Domain Route Meta Tag
===================

Domain Route Meta Tag module help users to set Meta tags for custom drupal urls.
Meta Title, Meta Description can be added with some extra fields provided.

Configuration:
--------------
Domain Route Meta Tag module have a form to add meta tags for drupal urls at:
/admin/config/system/domain_route_meta_tag/add.

List of added content may be found at:
/admin/config/system/domain_route_meta_tag/list

Dependencies:
-------------
This module depends on entity_reference, domain, options module.

Installation:
-------------
Install like any other module.

Notes:
------
This module best works for controller routes, views.
For nodes & taxonomies route use Meta Tags Module or Similar.
If domains are not listed in the form then, you have to clear drupal's cache.
