<?php

namespace Drupal\domain_route_meta_tag\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides a form for deleting a domain_route_meta_tag entity.
 *
 * @ingroup domain_route_meta_tag
 */
class DomainRouteMetaTagDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * Default Cache object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Current path object.
   *
   * @var object
   */
  protected $currentPath;

  /**
   * Logger Factory object.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Form constructor to define class variables.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache object.
   * @param object $currentPath
   *   Current path object.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   Entity repository object.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   Entity type bundle info object.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Date time object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger factory object.
   */
  public function __construct(CacheBackendInterface $cache,
  $currentPath,
  EntityRepositoryInterface $entity_repository,
  EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
  TimeInterface $time,
  LoggerChannelFactoryInterface $logger) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->cache = $cache;
    $this->currentPath = $currentPath;
    $this->logger = $logger->get('domain_route_meta_tag');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default'),
      $container->get('path.current'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->has('datetime.time') ? $container->get('datetime.time') : NULL,
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Domain Route Meta Entity ID [@id] deleted for @link',
      [
        '@link' => $this->entity->getRouteLink(),
        '@id' => $this->entity->id(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl() {
    return new Url('entity.domain_route_meta_tag.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. log() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $path = $this->getEntity()->get('route_link')->value;
    $cacheKey = str_replace('/', '_', $path);
    $cache = $this->cache->get($cacheKey);
    if ($cache) {
      $this->cache->delete($cacheKey);
    }
    $entity = $this->getEntity();
    $entity->delete();
    $this->logger->notice('Domain Route Meta Entity ID [@id] deleted for @link',
      [
        '@link' => $this->entity->getRouteLink(),
        '@id' => $this->entity->id(),
      ]
    );
    $this->messenger()->addMessage($this->t('Domain Route Meta Entity ID [@id] deleted for @link',
      [
        '@link' => $this->entity->getRouteLink(),
        '@id' => $this->entity->id(),
      ]
    ));
    $form_state->setRedirect('entity.domain_route_meta_tag.collection');
  }

}
