<?php

namespace Drupal\domain_route_meta_tag;

/**
 * Contains \Drupal\domain_route_meta_tag\DomainMetaTagInterface.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Custom meta entity.
 *
 * @ingroup domain_route_meta_tag
 */
interface DomainRouteMetaTagInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
