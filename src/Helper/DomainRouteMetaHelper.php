<?php

namespace Drupal\domain_route_meta_tag\Helper;

/**
 * Helper DomainRouteMetaHelper to handle common functionality.
 *
 * @package Drupal\modules\domain_route_meta_tag
 */
class DomainRouteMetaHelper {

  /**
   * Keep class object.
   *
   * @var object
   */
  public static $helper = NULL;

  /**
   * EntityTypeManager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * CurrentPath object.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Protected pathAlias variable.
   *
   * @var object
   */
  protected $pathAlias;

  /**
   * Protected negotiator variable.
   *
   * @var object
   */
  protected $negotiator;

  /**
   * Private constructor to avoid instantiation.
   */
  private function __construct() {
  }

  /**
   * Get class instance using this function.
   *
   * @return DomainRouteMetaHelper
   *   return Object.
   */
  public static function getInstance() {
    if (!self::$helper) {
      self::$helper = new DomainRouteMetaHelper();
      self::$helper->init();
    }
    return self::$helper;
  }

  /**
   * {@inheritdoc}
   *
   * Init call.
   */
  public function init() {
    $container = \Drupal::getContainer();
    $this->entityManager = $this->getMetaStorage($container);
    $this->currentPath = $this->getCurrentPath($container);
    $this->pathAlias = $this->getPathAlias($container);
    $this->negotiator = $this->getDomainNegotiator($container);
  }

  /**
   * Get Meta Storage.
   */
  private function getMetaStorage($container = NULL) {
    return $container->get('entity_type.manager')
      ->getStorage('domain_route_meta_tag');
  }

  /**
   * Get Current Path.
   */
  private function getCurrentPath($container = NULL) {
    return $container->get('path.current')->getPath();
  }

  /**
   * Get Path Alias.
   */
  private function getPathAlias($container = NULL) {
    return $container->get('path_alias.manager')
      ->getAliasByPath($this->currentPath);
  }

  /**
   * Get Domain Negotiator.
   */
  private function getDomainNegotiator($container = NULL) {
    return $container->get('domain.negotiator');
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaEntity() {
    $domain = NULL;
    $entity = NULL;
    $entityData = NULL;
    if ($this->negotiator->getActiveDomain() === NULL) {
      return $entity;
    }
    // Get Active Domain.
    $domain = $this->negotiator->getActiveDomain()->id();
    $values = [
      'route_link' => $this->pathAlias,
      'domain' => $domain,
    ];
    // Load entity by property.
    $entityData = $this->entityManager->loadByProperties($values);
    if (empty($entityData)) {
      $values['route_link'] = $this->currentPath;
      $entityData = $this->entityManager->loadByProperties($values);
    }
    // Check for entity data.
    if ($entityData) {
      foreach ($entityData as $entity) {
        break;
      }
    }
    return $entity;
  }

}
